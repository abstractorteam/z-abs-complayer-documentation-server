
'use strict';

const StackComponentsFactory = require('../../stack-components-factory');
const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class StyleGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    const index = this.expectAsynchResponse();
    let stacks = null;
    let stacksJson = null;
    let error = false;
    StackComponentsFactory.getStacks((s) => {
      stacks = s;
      this.analyze(stacks, stacksJson, index);
    });
    this.asynchReadFile(ActorPathData.getStacksLocalStyle(), (err, data) => {
      if(err && !error) {
        error = true;
        return this._asynchXErrorResponse(`Could read: '${ActorPathData.getStacksLocalStyle()}'`, err, index);
      }
      stacksJson = data;
      this.analyze(stacks, stacksJson, index);
    });
  }
  
  analyze(stacks, stacksJson, index) {
    if(null !== stacks && null !== stacksJson) {
      let indexStack = 0;
      const protocols = [
        ['unknown', {
          index: indexStack++,
          textColor: 'black',
          textProtocolColor: 'black',
          protocolColor: '#666',
          protocolBackgroundColor: 'rgba(102, 102, 102, 0.3)'
        }]
      ];
      stacks.forEach((path, stack) => {
        const currentStackApi = require(`${stack}-style`);
        const currentStyle = new currentStackApi(indexStack++);
        protocols.push([stack, currentStyle]);
      });
      return this._asynchXSuccessResponse(index, protocols);
    }
  }
}

module.exports = StyleGet;
