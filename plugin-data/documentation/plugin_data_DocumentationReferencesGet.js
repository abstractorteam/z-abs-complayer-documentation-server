
'use strict';

const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DocumentationReferencesGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(name) {
    this.asynchReadFileResponse(ActorPathDocumentation.getDocumentationReferences());
  }
}

module.exports = DocumentationReferencesGet;
