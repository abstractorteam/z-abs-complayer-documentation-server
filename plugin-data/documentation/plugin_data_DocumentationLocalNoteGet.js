
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DocumentationLocalNoteGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(guids, path) {
    if(!Array.isArray(guids)) {
      this.asynchReadTextFile(ActorPathData.getDocumentationNoteLocalFile(guids, path), (err, data) => {
        if(!err) {
          this.expectAsynchResponseSuccess({
            document: data,
            created: true,
            guid: guids
          });
        }
        else {
          this.expectAsynchResponseSuccess({
            document: DocumentationLocalNoteGet.NEW_NOTE,
            created: false,
            guid: guids
          });
        }
      });
    }
    else {
      let pendings = guids.length;
      if(0 === pendings) {
        process.nextTick(() => {
          this.expectAsynchResponseSuccess();
        });
        return;
      }
      const documents = [];
      guids.forEach((guid) => {
        this.asynchReadTextFile(ActorPathData.getDocumentationNoteLocalFile(guid, path), (err, data) => {
          if(!err) {
            documents.push({
              document: data,
              created: true,
              guid: guid
            });
          }
          else {
            documents.push({
              document: DocumentationLocalNoteGet.NEW_NOTE,
              created: false,
              guid: guid
            });
          }
          if(0 === --pendings) {
            this.expectAsynchResponseSuccess(documents);
          }
        });
      });
    }
  }
}

DocumentationLocalNoteGet.NEW_NOTE = '# NEW LOCAL NOTE\n## Please Edit: ***`TO BE DONE !!!`***\r\n';

module.exports = DocumentationLocalNoteGet;
