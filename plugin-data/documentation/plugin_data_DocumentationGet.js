
'use strict';

const StackHelper = require('./stack-helper');
const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');
const Fs = require('fs');


class DocumentationGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(appName, documentGroup, documentName, embed) {
    if(!embed) {
      this._getNavigationAndDocument(appName, documentGroup, documentName);
    }
    else {
      if(Array.isArray(documentName)) {
        this._getDocuments(documentGroup, documentName, DocumentationGet.NEW_EMBEDED_LINK);
      }
      else {
        this._getDocument(ActorPathDocumentation.getDocumentationGlobalFile(documentGroup, documentName), DocumentationGet.NEW_EMBEDED_LINK);
      }
    }
  }
  
  _getDocument(document, notFoundText) {
    const index1 = this.expectAsynchResponse();
    const index2 = this.expectAsynchResponse();
    Fs.readFile(document, 'utf8', (err, data) => {
      if(err) {
        this._asynchXSuccessResponse(index1, notFoundText);
        this._asynchXSuccessResponse(index2, false);
      }
      else {
        this._asynchXSuccessResponse(index1, data);
        this._asynchXSuccessResponse(index2, true);
      }
    });
  }
  
  _getDocuments(documentGroup, documentNames, notFoundText) {
    let pendings = documentNames.length;
    if(0 === pendings) {
      process.nextTick(() => {
        this.expectAsynchResponseSuccess();
      });
      return;
    }
    const index1 = this.expectAsynchResponse();
    const index2 = this.expectAsynchResponse();
    const index3 = this.expectAsynchResponse();
    const receivedDcuments = [];
    const documentNamesFound = [];
    const documentsFound = [];
    documentNames.forEach((documentName) => {
      const currentDocumentName = documentName;
      Fs.readFile(ActorPathDocumentation.getDocumentationGlobalFile(documentGroup, documentName), 'utf8', (err, data) => {
        documentNamesFound.push(currentDocumentName);
        if(err) {
          receivedDcuments.push(notFoundText);
          documentsFound.push(false);
        }
        else {
          receivedDcuments.push(data);
          documentsFound.push(true);
        }
        if(0 === --pendings) {
          this._asynchXSuccessResponse(index1, receivedDcuments);
          this._asynchXSuccessResponse(index2, documentNamesFound);
          this._asynchXSuccessResponse(index3, documentsFound);
        }
      });
    });  
  }
  
  _getNavigationAndDocument(appName, documentGroup, documentName) {
    const index1 = this.expectAsynchResponse();
    const match = documentName ? documentName.match(/^\[\[stack\-[a-z-]+\]\]/) : null;
    Fs.readFile(ActorPathDocumentation.getDocumentationNavigationGlobalFile(appName, documentGroup), (err, data) => {
      if(err) {
        this._asynchXErrorResponse(`Could not get DocumentationNavigationGlobalFile. '${ActorPathDocumentation.getDocumentationNavigationGlobalFile(appName, documentGroup)}'`, err, index1);
      }
      else {
        const navigations = JSON.parse(data);
        navigations.findIndex((navigation) => {
          const refIndex = navigation.refs.findIndex((ref) => {
            return ref.name === '[[stack]]';
          });
          if(-1 !== refIndex) {
            this._getStackNavigation(navigation, () => {
              if(documentName && match) {
                StackHelper.file(match, (file) => {
                  if(null !== file) {
                    this._getDocument(file, DocumentationGet.NEW_STACK);
                  }
                  else {
                    this._asynchXErrorResponse(`GetStacksObject not registered'.`, new Error('GetStacksObject not registered'), index1);
                  }
               });
              }
            });
            return true;
          }
          else {
            return false;
          }
        });
        if(!documentName) {
          if(0 !== navigations.length && navigations[0].refs.length) {
            this._getDocument(ActorPathDocumentation.getDocumentationGlobalFile(documentGroup, navigations[0].refs[0].link), DocumentationGet.NEW_DOCUMENT);
          }
          else {
            this.expectAsynchResponseSuccess(DocumentationGet.NEW_DOCUMENT);
            this.expectAsynchResponseSuccess(false);
          }
        }
        this._asynchXSuccessResponse(index1, navigations);
      }
    });
    if(documentName && !match) {
      this._getDocument(ActorPathDocumentation.getDocumentationGlobalFile(documentGroup, documentName), DocumentationGet.NEW_DOCUMENT);
    }
  }
  
  _getStackNavigation(navigation, done) {
    this.expectAsynchResponseTemp();
    navigation.refs = [];
    const stacksObject = StackHelper.getStacksObject();
    if(undefined !== stacksObject) {
      StackHelper.getStacksObject().getStacks((stacks) => {
        stacks.forEach((value, key) => {
          navigation.refs.push({
            name: key,
            link: `[[stack-${key}]]`
          });
        });
        done();
        this.unExpectAsynchResponseTemp();
      });
    }
    else {
      done();
      this.unExpectAsynchResponseTemp();
    }
  }
}

DocumentationGet.NEW_DOCUMENT = '# **NEW DOCUMENT**\n## Please Edit: ***`TO BE DONE !!!`***\r\n';
DocumentationGet.NEW_EMBEDED_LINK = '## **EMBEDED LINK NOT CREATED YET.**\r\n';
DocumentationGet.NEW_STACK = '## **STACK DOCUMENTATION NOT CREATED YET**\n## Please Edit: ***`TO BE DONE !!!`***\r\n';


module.exports = DocumentationGet;
