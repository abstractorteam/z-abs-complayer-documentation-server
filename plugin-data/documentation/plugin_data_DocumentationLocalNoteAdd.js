
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DocumentationLocalNoteAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(guid, path, document) {
    let pathAndFile = ActorPathData.getDocumentationNoteLocalFileAndPath(guid, path);
    this.asynchMkdirResponse(pathAndFile.path, (err) => {
      if(!err) {
        this.asynchWriteTextFileResponse(pathAndFile.file, document);
      }
      else {
        this.responsePartError(`Could not create document path '${pathAndFile.file}'.`, err);
      }
    }, true);
  }
}

module.exports = DocumentationLocalNoteAdd;
