
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class DocumentationLocalNoteUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(guid, path, document) {
    this.asynchWriteTextFileResponse(ActorPathData.getDocumentationNoteLocalFile(guid, path), document);
  }
}

module.exports = DocumentationLocalNoteUpdate;
