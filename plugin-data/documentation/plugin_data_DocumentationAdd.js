
'use strict';

const StackHelper = require('./stack-helper');
const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


// TODO: synchronize access to the file
class DocumentationAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(documentGroup, documentName, documentData) {
    const match = documentName ? documentName.match(/^\[\[stack\-[a-z-]+\]\]/) : null;
    if(!match) {
      this._save(ActorPathDocumentation.getDocumentationGlobalFilePathAndFile(documentGroup, documentName), documentName, documentData);
    }
    else {
      StackHelper.pathAndFile(match, (pathAndFile) => {
        this._save(pathAndFile, documentName, documentData);
      });
    }
  }
  
  _save(pathAndFile, documentName, documentData) {
    if(null !== pathAndFile) {
      this.asynchMkdirResponse(pathAndFile.path, (err) => {
        if(!err) {
          this.asynchWriteTextFileResponse(pathAndFile.file, documentData);
        }
        else {
          this.responsePartError(`Could not create document path '${documentName}'.`, err);
        }
      }, true);
    }
    else {
      this.responsePartError(`GetStacksObject not registered'.`, new Error('GetStacksObject not registered'));
    }
  }
}

module.exports = DocumentationAdd;
