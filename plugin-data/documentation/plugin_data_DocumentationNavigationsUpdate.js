
'use strict';

const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');
const Fs = require('fs');


class DocumentationNavigationsUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(appName, documentGroup, navigations) {
    Fs.writeFile(ActorPathDocumentation.getDocumentationNavigationGlobalFile(appName, documentGroup), JSON.stringify(navigations, null, 2), (err) => {
      if(err) {
        return this.responsePartError(`Could not update the Document Navigation File: ' ${ActorPathDocumentation.getDocumentationNavigationGlobalFile(appName, documentGroup)} '.`, err);
      }
      return this.responsePartSuccess();
    });
  }
}

module.exports = DocumentationNavigationsUpdate;
