
'use strict';

const ActorPathData = require('z-abs-corelayer-server/path/actor-path-data');
const ActorPathDist = require('z-abs-corelayer-server/path/actor-path-dist');
const Path = require('path');


class StackHelper {
  static file(match, done) {
    const key = match[0].substring('[[stack-'.length, match[0].length - 2);
    const path = match.input.substring(match[0].length);
    if(null !== StackHelper.getStacksObject()) {
      StackHelper.getStacksObject().getStacks((stacks) => {
        const distPath = stacks.get(key).path;
        let basePath = '';
        let index = distPath.indexOf(ActorPathDist.getActorDistStacksLocalPath());
        if(-1 !== index) {
          basePath = distPath.replace(ActorPathDist.getActorDistStacksLocalPath(), ActorPathData.getStacksLocalFolder());
        }
        else {
          index = distPath.indexOf(ActorPathDist.getActorDistStacksGlobalPath());
          if(-1 !== index) {
            basePath = distPath.replace(ActorPathDist.getActorDistStacksGlobalPath(), ActorPathData.getStacksGlobalFolder());
          }
          else {
            // TODO: handle error. 
          }
        }
    
        if(0 === path.length) {
          done(`${basePath}${Path.sep}documentation${Path.sep}${key}.md`);
        }
        else if(1 === path.length) {
          if('/' === path[0]) {
            done(`${basePath}${Path.sep}documentation${Path.sep}${key}.md`);
          }
          else {
            done(`${basePath}${Path.sep}documentation${Path.sep}${key}${path}.md`);
          }
        }
        else {
          done(`${basePath}${Path.sep}documentation${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}.md`);
        }
      });
    }
    else {
      done(null);
    }
  }
  
  static pathAndFile(match, done) {
    StackHelper.file(match, (file) => {
      if(null !== file) {
        const lastIndex = file.lastIndexOf(Path.sep);
        done({
          path: file.substring(0, lastIndex),
          file: file
        });
      }
      else {
        done(null);
      }
    });
  }
  
  static registerGetStacksObject(getStacksObject) {
    Reflect.set(global, 'getStacksObject@abstractor', getStacksObject);
  }
  
  static getStacksObject() {
    return Reflect.get(global, 'getStacksObject@abstractor');
  }
}


module.exports = StackHelper;
