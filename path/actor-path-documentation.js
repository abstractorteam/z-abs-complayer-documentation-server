
'use strict';

const Path = require('path');
const ActorPath = require('z-abs-corelayer-server/path/actor-path');


class ActorPathDocumentation {
  constructor() {
    this.actorDocumentation  = ActorPath.setPath('ActorDocumentationFolder', ActorPath.getActorPath(), `${Path.sep}..${Path.sep}Documentation`);
    this.actorDocumentationText  = ActorPath.setPath('ActorDocumentationTextFolder', this.actorDocumentation, `${Path.sep}actorjs-documentation-text`);
    this.actorDocumentationBin  = ActorPath.setPath('ActorDocumentationBinFolder', this.actorDocumentation, `${Path.sep}actorjs-documentation-bin`);
  }
  
  getDocumentationReferences() {
    return `${this.actorDocumentationText}${Path.sep}Documentation${Path.sep}documentation-references.json`;
  }

  getDocumentationGlobalFolder(documentGroup) {
    return `${this.actorDocumentationText}${Path.sep}${documentGroup}`;
  }
  
  getDocumentationNavigationGlobalFile(appName, documentGroup) {
    return `${this.getDocumentationGlobalFolder(documentGroup)}${Path.sep}Navigation-${documentGroup}-${appName}-global.txt`;
  }
  
  getDocumentationGlobalFilePathAndFile(documentGroup, documentName) {
    let formattedDocumentName = Path.normalize(documentName.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
    let pathSplit = formattedDocumentName.split(Path.sep);
    let splitteredDocumentName = pathSplit[pathSplit.length - 1];
    let splitteredPath = '';
    if(1 !== pathSplit.length) {
      pathSplit.splice(pathSplit.length - 1, 1);
      splitteredPath = pathSplit.join(Path.sep);
    }
    return {
      path: `${this.actorDocumentationText}${Path.sep}${documentGroup}${Path.sep}${splitteredPath}`,
      file: `${this.actorDocumentationText}${Path.sep}${documentGroup}${Path.sep}${splitteredPath}${Path.sep}Document-global-${splitteredDocumentName}.txt`
    };
  }
  
  getDocumentationGlobalFile(documentGroup, documentName) {
    return this.getDocumentationGlobalFilePathAndFile(documentGroup, documentName).file;
  }
}


module.exports = new ActorPathDocumentation();
