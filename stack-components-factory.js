
'use strict';
// TODO: double code
const StackHelper = require('./plugin-data/documentation/stack-helper');
const ActorPathDist = require('z-abs-corelayer-server/path/actor-path-dist');
const Fs = require('fs');
const Path = require('path');


class StackComponentsFactory {
  constructor() {
    this.stacks = new Map();
    this.stacksList = [];
    this.loaded = false;
    this.stacksQueue = [];
    this.findStacks((err) => {
      if(err) {
        console.log('Error loading StackComponentsFactory', err);
      }
      this.registerStacks();
      this.loaded = true;
      while(0 !== this.stacksQueue.length) {
        this.stacksQueue.shift()(this.stacks);
      }
    });
    StackHelper.registerGetStacksObject(this);
  }
  
  /*createClient(stackName) {
    const stackPath = this.stacks.get(stackName);
    if(undefined !== stackPath) {
      return stackPath.client;
    }
    else {
      throw(new Error(`Client stack: '${stackName}' does not exist.`));
    }
  }
  
  createServer(stackName) {
   const stackPath = this.stacks.get(stackName);
    if(undefined !== stackPath) {
      return stackPath.server;
    }
    else {
      throw(new Error(`Server stack: '${stackName}' does not exist.`));
    }
  }*/
  
  getStacks(done) {
    this.stacksQueue.push(done);
    if(this.loaded) {
      while(0 !== this.stacksQueue.length) {
        this.stacksQueue.shift()(this.stacks);
      }
    }
  }
 
  registerStacks() {
    if(1 <= this.stacks.size) {
      const paths = [ActorPathDist.actorDistActorApiPath, ActorPathDist.actorDistStackApiPath];
      this.stacks.forEach((stack) => {
        paths.push(stack.path);
      });
      const newPaths = [];
      const delimiter = 'win32' === process.platform ? ';' : ':';
      const nodePaths = process.env.NODE_PATH.split(delimiter);
      paths.forEach((path) => {
        if(-1 === nodePaths.indexOf(path)) {
          newPaths.push(path);
        }
      });
      if(0 !== newPaths.length) {
        if(undefined !== process.env.NODE_PATH) {
          process.env.NODE_PATH = `${process.env.NODE_PATH}${delimiter}${newPaths.join(delimiter)}`;
          require('module').Module._initPaths();
        }
        else {
          process.env.NODE_PATH = `${newPaths.join(delimiter)}`;
          require('module').Module._initPaths();
        }
      }
    }
  }
  
  sortStacks() {
    this.stacksList.sort((a, b) => {
      if(a[0] > b[0]) {
        return 1;
      }
      else if(a[0] < b[0]) {
        return -1;
      }
      else {
        return 0;
      }
    });
    this.stacks = new Map(this.stacksList);
  }
  
  findStacks(done) {
    let pendings = 2;
    let isError = false;
    this._findStacks(ActorPathDist.getActorDistStacksGlobalPath(), (err) => {
      if(!isError) {
        if(err) {
          isError = true;
          done(err);
        }
        else if(0 === --pendings) {
          this.sortStacks();
          return done();
        }
      }
    });
    this._findStacks(ActorPathDist.getActorDistStacksLocalPath(), (err) => {
       if(!isError) {
        if(err && 'ENOENT' !== err.code) {
          isError = true;
          done(err);
        }
        else if(0 === --pendings) {
          this.sortStacks();
          return done();
        }
      } 
    });
  }
  
  _findStacks(loadPath, done) {
    Fs.readdir(loadPath, (err, files) => {
      if(err) {
        return done(err);
      }
      if(undefined === files || 0 === files.length) {
        return done();
      }
      let firstError;
      let pendings = files.length;
      files.forEach((dir) => {
        const path = `${loadPath}${Path.sep}${dir}`;
        Fs.lstat(path, (err, stat) => {
          if(undefined === firstError) {
            if(stat.isDirectory()) {
              try {
                this.stacksList.push([dir, {
                  name: dir,
                  path: path,
                  client: '',
                  server: ''
                }]);
              }
              catch(errCatch) {
                done(errCatch);
                // TODO: addlog error here.
              }
            }
            if(0 === --pendings) {
              done();
            }
          }
        });
      });
    });
  }
}


module.exports = new StackComponentsFactory();
